@extends('layout.master')

@section('judul')
Media Online 
@endsection

@section('content')
    
<h2>Sosial Media Developer</h2>
<p>Belajar dan berbagi agar hidup menjadi lebih baik</p>

<h3>Benefit join di media online</h3>
<ul>
    <li>Mendapatkan motivasi dari sesama developer</li>
    <li>Sharing Knowledge</li>
    <li>Dibuat oleh calon web developer terbaik</li>
</ul>

<h3>Cara bergabung ke media online</h3>
<ol>
    <li>Mengunjungi website ini</li>
    <li>Mendaftarkan di <a href="/register">Form sign up</a> </li>
    <li>Selesai</li>
</ol>

@endsection