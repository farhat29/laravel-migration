<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('home.register');
} 

    public function welcome(Request $request){
        // dd($request->all());
        $nama = $request->nama;
        $biodata = $request-> bio;

        return view('home.welcome', compact('nama', 'biodata'));
    }

}
